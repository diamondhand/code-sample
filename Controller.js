/**
 * ABookImportV3 Controller
 *
 * Controller for ABookImportV3.
 *
 * The controller handle the communication among ABookImportV3 and its external modules such as:
 * ABookFetchForm,
 *
 *
 * Dependencies:
 *  LI.MediaServerUploader
 *  LI.FetchForm (v3)
 *  LI.FetchFormView (v3)
 *  LI.ImportEmailResolver (v3)
 *  LI.ImportSubmitter (v3)
 *
 * @class ABookImportV3.Controller
 * @constructor
 * @param {Object} config
 **/

LI.define('ABookImportV3.Controller');
LI.ABookImportV3.Controller =  function(config) {
  var
    CONST           = config.consts || {},
    LI_EVENTS       = config.events || {},
    _model          = config.model || {},
    _view           = config.view || {},
    _helpers        = config.helpers || {},
    _webTracking    = config.webTracking || {},
    _mesg           = config.mesg || {},
    _externalJSControls = config.externalJSControls || {},
    _events         = CONST.EVENTS,
    _attr           = CONST.ATTR,
    _states         = CONST.STATES,
    _css            = CONST.CSS,
    _fileUploader,
    _externalEvents,
    _defExtEvents   = {
      showFetchForm:            'LI.FetchForm.SHOW_ON_DEMAND',
      submitFetchForm:          'LI.FetchForm.SUBMIT_ON_DEMAND',
      resetFetchForm:           'LI.FetchForm.RESET_FORM_ON_DEMAND',
      submittedFetchForm:       'LI.FetchForm.SUBMITTED',
      asyncInProgressFetchForm: 'LI.FetchForm.ASYNC_IMPORT_IN_PROGRESS',
      startResolveFetchForm:    'startResolve',
      loadPymkModule:           'abi-pymk-load',
      mediaUpload:              'mpr-upload-start',
      mediaUploadReturned:      'mpr-upload-returned'
    },
    $abookImportForm      = $('#' + _css.ID_ABOOK_IMPORT_FORM),
    $abookImportContainer = $('.' + _css.CLASS_ABOOK_IMPORT_CONTAINER),
    $emailOriginContainer = $('#' + _css.ID_EMAIL_ORIGIN_CONTAINER),
    $classAddConns        = $('.' + _css.CLASS_ADD_CONNECTIONS),
    $closeButtons         = $('#' + _css.ID_AUX_CONTAINER + ' .' + _css.CLASS_CLOSE),
    $uploadFile           = $('#' + _css.ID_UPLOAD_FILE),
    $invitationList       = $('#' + _css.ID_INVITATION_LIST),
    $body                 = $('body'),
    $uploadFileForm       = $('#' + _css.ID_UPLOAD_FILE_FORM),
    $selectFileEl         = $uploadFileForm.find('.'+_css.CLASS_SELECT_FILE),

    //DOM EVENTS
    CLICK = 'click',
    MOUSE_ENTER = 'mouseenter',
    BLUR = 'blur',
    FOCUS = 'focus',

    //TRACKIGN CODES
    TRACK_CLOSE_AUX     = 'fetch-abookimprt3-close-container',
    TRACK_CLICKING_CARD = 'fetch-abookimprt3-click-card';

  config.fileUploader   = config.fileUploader || null;
  config.externalEvents = config.externalEvents || _defExtEvents;
  _externalEvents = config.externalEvents;


  /**
   * Initializes the ABookImportV3
   *
   * @method: init
   * @public
   *
   */
  this.init = function () {
    this._initJSControls();
    _model.setStateToInit();
    this._setUpCustomEvents();
    this._attachEventListeners();
    LI_EVENTS.trigger(_events.RESET);
  };


  /**
   * Reinitializes requried JS controls if not already present.
   *
   * This is necessary because #abook-import-form's dom node gets
   * moved around. Without the forced reinit, if the browser navigates
   * away to a diff page and comes back, one of the js controls, LI.ABookFetchForm,
   * will not get reinited thru JSControls' framework.
   *
   * @method: _initJSControls
   * @private
   *
   */
  this._initJSControls = function () {
    var control;

    for (var i = 0; i < _externalJSControls.length ; i++) {
      control = LI.Controls.getControl(_css.ID_EMAIL_ORIGIN_CONTAINER, _externalJSControls[i]);
      if (!control) {
        LI.Controls.parseFragment($emailOriginContainer[0]);
      }
    }
  };

  /**
   * Set up custom events for model, view, external modules
   *
   * @method: _setUpCustomEvents
   * @private
   *
   */
  this._setUpCustomEvents = function () {

    //Create Aux Container
    LI_EVENTS.on(_events.CREATE_AUX_CONTAINER, _view.createAux);
    LI_EVENTS.on(_events.CREATE_AUX_CONTAINER, function(){
      LI_EVENTS.trigger(_externalEvents.showFetchForm);
    });

    //Destroy Aux Container
    LI_EVENTS.on(_events.DESTROY_AUX_CONTAINER, _view.disableAux);

    //ABook is loading in stuff
    LI_EVENTS.on(_events.LOADING, _model.setStateToLoading);
    LI_EVENTS.on(_events.LOADING, _view.showLoading);

    //ABook has finished loading in stuff
    LI_EVENTS.on(_events.LOADED, _model.setStateToLoaded);
    LI_EVENTS.on(_events.LOADED, _view.hideLoading);

    //ABook has encountered an error
    LI_EVENTS.on(_events.ERROR, _model.setStateToError);
    LI_EVENTS.on(_events.ERROR, _view.hideLoading);
    LI_EVENTS.on(_events.ERROR, function(err){
      _view.showError(err.$node);
      _view.injectMessage(err.mesg);
    });

    //ABook Submit
    LI_EVENTS.on(_events.SUBMIT, _view.removeAllErrors);
    LI_EVENTS.on(_events.SUBMIT, this._processSubmit);

    //Reset everything
    LI_EVENTS.on(_events.RESET, _model.setStateToLoaded);
    LI_EVENTS.on(_events.RESET, _view.disableAux);
    LI_EVENTS.on(_events.RESET, _view.hideLoading);
    LI_EVENTS.on(_events.RESET, _view.removeAllErrors);
    LI_EVENTS.on(_events.RESET, _view.resetFileUpload);
    LI_EVENTS.on(_events.RESET, _view.resetInvitationList);

    LI_EVENTS.on(_events.RESET, function() {
      _view.displayFileUpload({ 'title': _mesg.selectFiles || '' });
    });

    //Start the entertainment module -- PYMK!
    LI_EVENTS.on(_events.START_ENTERTAINMENT, _model.setStateToEntertainment);
    LI_EVENTS.on(_events.START_ENTERTAINMENT, _view.displayEntertainment);
    LI_EVENTS.on(_events.START_ENTERTAINMENT, _view.showLoading);
    LI_EVENTS.on(_events.START_ENTERTAINMENT, function(){
      LI_EVENTS.trigger(_externalEvents.loadPymkModule);
    });

    //After fetch form is submitted, clear out forms. reset!
    LI_EVENTS.on(_externalEvents.submittedFetchForm, this._processSubmittedFetchForm);

    //Fetchform is loading async stuff. This will take some time, kick in the entertainment module
    LI_EVENTS.on(_externalEvents.asyncInProgressFetchForm, function(){
      LI_EVENTS.trigger(_events.START_ENTERTAINMENT);
    });
  };


  /**
   * Attach DOM Event Listeners
   *
   * @method: _attachEventListeners
   * @private
   *
   */
  this._attachEventListeners = function() {
    //listen to click on cards
    $emailOriginContainer.on(CLICK, $.proxy(this._onClickingCard, this));

    //listen to mouse enter of num of conns
    $classAddConns.on(MOUSE_ENTER, $emailOriginContainer, $.proxy(this._onMousingOverAddConns, this));

    //listen to close button
    $closeButtons.on(CLICK, $.proxy(this._onClickingCloseButton, this));

    //listen to file upload change
    $uploadFile.on('change', $.proxy(this._onFileUploadChange, this))
      // For accessibility, add focus to faux span when input gets focus and vice versa onBlur
      .on(FOCUS, function() {
        $selectFileEl.addClass(FOCUS);
      })
        .on(BLUR, function() {
          $selectFileEl.removeClass(FOCUS);
        });

    //listen to the invitation list text area
    $invitationList.on(BLUR, $.proxy(this._onInvitationList, this));

    //listen to esc key
    $(document).keyup($.proxy(this._onKeydownEsc, this));
  };


  /**
   * Processes FetchForm after form submission
   *
   * @method: _processSubmittedFetchForm
   * @private
   * @param {Object} emailOrigin
   */
  this._processSubmittedFetchForm = function(emailOrigin){
    if (emailOrigin) {
      if (emailOrigin.async !== null) {
        //this is necessary because user may submit non-async email origin inside a card intended for async email
        _model.setAsync(emailOrigin.async);
      }
    }

    if (!_model.getAsync()){
      LI_EVENTS.trigger(_events.RESET);
    }
  };

  /**
   * Handler for a click on one of the cards
   *
   * @method: _onClickingCard
   * @private
   * @param {Object} evt
   */
  this._onClickingCard = function (evt) {
    var cardNode    = $(evt.target).closest( '.' + _css.CLASS_CARD)[0],
        evtTarget   = evt.target,
        $evtTarget  = $(evtTarget),
        $form       = $evtTarget.closest('form'),
        input       = 'INPUT',
        button      = 'BUTTON',
        submit      = 'SUBMIT';

    if(evtTarget.nodeName === input && evtTarget.type.toUpperCase() === submit) { //user clicking on SUBMIT

      LI_EVENTS.trigger(_events.SUBMIT, {$form: $form, inputEvt: evt});

    } else { //clicking on any other elements within card itself

      if(evtTarget.nodeName !== button && evtTarget.nodeName !== input) {
        evt.preventDefault();
      }

      if ($evtTarget.hasClass(_css.CLASS_POP_UP)) {
        _view.showPopUp($evtTarget);
      } else {
        this._processCard(cardNode);
      }
    }
  };

  /**
   * Processes the card after it's clicked -- checking if it's an
   * oAuth card or non-oAuth card
   *
   * @method: _processCard
   * @private
   * @param {Object} cardNode
   */
  this._processCard = function (cardNode) {

    if (!_model.isLoading() && !_model.isInEntertainment()) { //make sure not in loading state before proceeding
      if ( _helpers.isCard(cardNode) ) {
        _webTracking.trackUserAction(TRACK_CLICKING_CARD + '-' +  _model.getEmailOrigin());
        _model.setModel(cardNode);

        if ( _model.getOAuth() !== 'true' ) { //non oAuth
          this._processNonOAuthSelection();
        } else { //oAuth
          this._processOAuthSelection();
        }
      }
    }
  };


  /**
   * Processes oAuth card selection. Will auto submit via fetchform to
   * get the popup.
   *
   * @method: _processOAuthSelection
   * @private
   */
  this._processOAuthSelection = function() {
    var originData = _model.getEmailOriginData();

    // If a link is provided, redirect user - used for manage contacts redirect
    // if user has had a recent OAuth import
    if (originData.linkTo.length) {
      window.location = originData.linkTo;
      return;
    }

    if (!originData.email) {
      originData.email = _model.getDummyEmail(); //this is for oauth buttons
    }

    LI_EVENTS.trigger(_events.DESTROY_AUX_CONTAINER);
    LI_EVENTS.trigger(_externalEvents.submitFetchForm, originData);
  };


  /**
   * Processes non oauth selection. Creates the auxiliary
   * container that hosts many different kinds of content that could
   * be file upload, free form text or fetchform
   *
   * @method: _processNonOAuthSelection
   * @private
   */
  this._processNonOAuthSelection = function() {
    var originData = _model.getEmailOriginData();
    LI_EVENTS.trigger(_events.CREATE_AUX_CONTAINER, originData);
  };


  /**
   * Mouse over callback on "add connections" box of top right corner of the card.
   *
   * @method: _onMousingOverAddConns
   * @private
   * @param {Object} evt
   */
  this._onMousingOverAddConns = function(evt) {
    var target = evt.target;
    _view.showAddConnectionsTooltip(target);
  };


  /**
   * Clicking close button on the aux container
   *
   * @method: _onClickingCloseButton
   * @private
   * @param {Object} evt
   */
  this._onClickingCloseButton = function(evt) {
    evt.preventDefault();
    this._processCloseAux();
  };


  /**
   * Escape key detection. Closes aux container.
   *
   * @method: _onKeydownEsc
   * @private
   * @param {Object} evt
   */
  this._onKeydownEsc = function(evt){
    if (evt.keyCode === 27) {
      this._processCloseAux();
    }
  };



  /**
   * Processes AUX container closing
   *
   * @method: _processCloseAux
   * @private
   *
   */
  this._processCloseAux = function(){
    if (!_model.isInEntertainment()){
      if (!_model.isLoading()){
        LI_EVENTS.trigger(_events.RESET);
      }

      LI_EVENTS.trigger(_externalEvents.resetFetchForm);
      _webTracking.trackUserAction(TRACK_CLOSE_AUX);
    }
  };


  /**
   * User selects different file for file/contacts upload. need to show UI changes.
   * Move this into View.
   *
   * @method: _onFileUploadChange
   * @private
   * @param {Object} evt
   */
  this._onFileUploadChange = function(evt) {
    var $node = $(evt.target),
        fileName;

    fileName = $node.val().split('\\');
    fileName = fileName[fileName.length - 1];

    _view.displayFileUpload({
      'fileName': fileName
    });

    this._setupFileUploader();
  };

  /**
   * Setup the file uploader using LI.MediaServerUploader.
   * Hooking into MediaServerUploader's events
   *
   * @method: _setupFileUploader
   * @private
   */
  this._setupFileUploader = function() {

    if (!_fileUploader) {
      _fileUploader = new config.fileUploader($uploadFileForm[0], {iframeId: _css.ID_MEDIA_UPLOADER});
    }

    $uploadFileForm.on(_externalEvents.mediaUpload, function(){
      LI_EVENTS.trigger(_events.LOADING);

      // Add loader
      _view.toggleLoading($uploadFileForm, true);
    });

    $uploadFileForm.on(_externalEvents.mediaUploadReturned, function(handler, response){
      var resObj = $.parseJSON(response.html);

      if (resObj && resObj.status === 'SUCCESS') {
        window.location = $uploadFileForm.attr(_attr.DATA_LI_UPLOAD_SUCCESS_URL); //redirect to results page

      } else { //failed
        // Remove loader
        _view.toggleLoading($uploadFileForm);

        LI_EVENTS.trigger(_events.ERROR, {
          $node: $uploadFileForm,
          mesg: _mesg.fileUploadError
        });
      }
    });
  };

  /**
   * Disables manual import invitation list until text-area has a value.
   *
   * @method _onInvitationList
   * @private
   */
  this._onInvitationList = function() {
    var hasVal = ($invitationList.val()) ? true : false;
    _view.toggleInvitationList(hasVal);
  };

  /****
   *
   * Processes Submit Event (non-fetchForm ones)
   *
   * @method: _processSubmit
   * @private
   * @param: {Object} Contains inputEvt (form input event) and $form (form jquery obj)
   */
  this._processSubmit = function (data) {
    if (!_helpers.validateForm(data.$form)){
      data.inputEvt.preventDefault();
      LI_EVENTS.trigger(_events.ERROR, {
        $node: data.$form
      });
    }
  };

  //this kickstarts the entire ABookImportV3
  this.init();
};
