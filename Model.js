/**
 * Model of ABookImportV3
 *
 * Stores:
 *  1 - The card which user clicked on (attributes contain:
 *      email origin, oAuth, isAsync, dummy email)
 *
 *  2 - Which state ABookImport is in: INITED, LOADING, LOADED, ENTERTAINMENT
 *
 * @class ABookImportV3.Model
 * @constructor
 **/

LI.define('ABookImportV3.Model');

(function(config) {

  function Model(config) {

    var CONST     = config.consts,
        _states   = CONST.STATES,
        _attr     = CONST.ATTR,
        self      = this;

    /**
     * Sets the model with data provided from a card element
     *
     * @method: setModel
     * @public
     * @param {Object} cardNode
     */
    this.setModel = function(cardNode) {
      var $card = $(cardNode);

      self.setCard(cardNode);
      self.setEmailOrigin($card.attr(_attr.DATA_LI_EMAIL_ORIGIN));
      self.setOAuth($card.attr(_attr.DATA_LI_IS_OAUTH));
      self.setAsync($card.attr(_attr.DATA_LI_IS_ASYNC));
      self.setDummyEmail($card.attr(_attr.DATA_LI_DUMMY_EMAIL));
      self.setLinkTo($card.attr(_attr.DATA_LI_LINK_TO));
    };


    /**
     * Builds and returns an email origin data object.
     *
     * @method: buildEmailOriginData
     * @public
     * @return {Object}
     */
    this.getEmailOriginData = function () {
      var originData = {
        emailOrigin: self.getEmailOrigin(),
        oAuth:       self.getOAuth(),
        async:       self.getAsync(),
        card:        self.getCard(),
        dummyEmail:  self.getDummyEmail(),
        linkTo:      self.getLinkTo()
      };
      return originData;
    };


    /**
     *
     * @method: setEmailOrigin
     * @public
     * @param {Object} emailOrigin
     */
    this.setEmailOrigin = function(emailOrigin) {
      if (emailOrigin !== self.getEmailOrigin()) {
        self.emailOrigin = emailOrigin;
      }
    };


    /**
     *
     * @method: setCard
     * @public
     * @param {Object} domNode
     */
    this.setCard = function(domNode) {
      if (domNode !== self.getCard() ) {
        self.card = domNode;
      }
    };


    /**
     *
     * @method: setState
     * @public
     * @param {String}
     */
    this.setState = function(state) {
      self.state = state;
    };

    /**
     * async attribute can be set:
     *
     * 1. At the time the user clicks on a card
     * 2. When user clicks on submit on the fetchform.
     *
     * The reason for that is because the user can enter
     * an non-async email address in the fetchform while
     * the fechtform is inside an Outlook card (a supposed async service)
     *
     * @method: setAsync
     * @public
     * @param {Boolean}
     */
    this.setAsync = function(isAsync) {
      self.async = isAsync;
    };

    /**
     *
     * @method: setOAuth
     * @public
     * @param {Boolean}
     */
    this.setOAuth = function(isOAuth) {
      self.oAuth = isOAuth;
    };

    /**
     *
     * @method: setStateToLoading
     * @public
     */
    this.setStateToLoading = function() {
      self.setState(_states.LOADING);
    };

    /**
     *
     * @method: setStateToLoaded
     * @public
     */
    this.setStateToLoaded = function() {
      self.setState(_states.LOADED);
    };

    /**
     *
     * @method: setStateToError
     * @public
     */
    this.setStateToError = function() {
      self.setState(_states.ERROR);
    };

    /**
     *
     * @method: setStateToEntertainment
     * @public
     */
    this.setStateToEntertainment = function() {
      self.setState(_states.ENTERTAINMENT);
    };


    /**
     *
     * @method: setStateToInit
     * @public
     */
    this.setStateToInit = function() {
      self.setState(_states.INIT);
    };


    /**
     *
     * @method: setDummyEmail
     * @public
     * @param {String}
     */
    this.setDummyEmail = function(dummyEmail) {
      self.dummyEmail = dummyEmail;
    };

    /**
     *
     * @method: setLinkTo
     * @public
     * @param {String}
     */
    this.setLinkTo = function(linkTo) {
      self.linkTo = linkTo;
    };

    /**
     *
     * @method: getState
     * @public
     * @return {String}
     */
    this.getState = function() {
      return self.state;
    };


    /**
     *
     * @method: getDummyEmail
     * @public
     * @return {String}
     */
    this.getDummyEmail = function() {
      return self.dummyEmail;
    };


    /**
     *
     * @method: getLinkTo
     * @public
     * @return {String}
     */
    this.getLinkTo = function() {
      return self.linkTo || '';
    };



    /**
     *
     * @method: getAsync
     * @public
     * @return {String}
     */
    this.getAsync = function() {
      return self.async;
    };

    /**
     *
     * @method: getOAuth
     * @public
     * @return {String}
     */
    this.getOAuth = function() {
      return self.oAuth;
    };

    /**
     *
     * @method: getEmailOrigin
     * @public
     * @return {String}
     */
    this.getEmailOrigin = function() {
      return self.emailOrigin;
    };


    /**
     *
     * @method: getCard
     * @public
     * @return {Object} element
     */
    this.getCard = function() {
      return self.card;
    };


    /**
     *
     * @method: isLoading
     * @public
     * @return {String}
     */
    this.isLoading = function() {
      return (this.getState() === _states.LOADING);
    };

    /**
     *
     * @method: isInEntertainment
     * @public
     * @return {String}
     */
    this.isInEntertainment= function() {
      return (this.getState() === _states.ENTERTAINMENT);
    };


    /**
     *
     * @method: isLoaded
     * @public
     * @return {String}
     */
    this.isLoaded = function() {
      return (this.getState() === _states.LOADED);
    };
  }

  LI.ABookImportV3.Model = new Model(config);

})({consts: LI.ABookImportV3.Const});

