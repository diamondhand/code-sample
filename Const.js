/**
 * Constant Hash Object for ABookImportV3
 *
 *
 * @class ABookImportV3.Const
 * @Object
 *
 **/

LI.define('ABookImportV3.Const');

LI.ABookImportV3.Const = {

  /*
   *************************
   *
   * CSS Classes and IDs
   *
   *************************
   */
  CSS: {
    CLASS_DISABLE_UI                   : 'disable-ui',
    CLASS_CARD                         : 'card',
    CLASS_CARD_STATUS                  : 'status',
    CLASS_CARD_TOOLTIP                 : 'tooltip',
    CLASS_ACTIONS                      : 'actions',
    CLASS_CLOSE                        : 'close',
    CLASS_AUX_TEMPLATE                 : 'aux-template',
    CLASS_IMPORT_INVITE_FORM_MODULE    : 'import-invite-form',
    CLASS_COMPOSE_INVITE_MODULE        : 'compose-invite',
    CLASS_CONTACTS_FILE_MODULE         : 'contacts-file',
    CLASS_OTHER_EMAILS_MODULE          : 'other-emails',
    CLASS_EXCHANGE_MODULE              : 'webmailImportExchange',
    CLASS_IMPORTING_IN_PROGRESS_MODULE : 'importing-in-progress-module',
    CLASS_PYMK_MODULE                  : 'pymk-module',
    CLASS_MODULE                       : 'module',
    CLASS_CONTENT                      : 'content',
    CLASS_ADD_CONNECTIONS              : 'add-connections',
    CLASS_ADD_CONNECTIONS_BUTTON       : 'add-connections-button',
    CLASS_LAST_HIGHLIGHTED             : 'last-highlighted',
    CLASS_LOADING                      : 'loading',
    CLASS_POP_UP                       : 'pop-up',
    CLASS_SELECT_FILE                  : 'select-file',
    CLASS_ON                           : 'on',
    CLASS_ENTERTAINMENT                : 'entertainment',
    CLASS_EMAIL_ORIGIN                 : 'email-origin',
    CLASS_ABOOK_IMPORT_CONTAINER       : 'abook-imprt-container',
    CLASS_ERROR                        : 'error',
    CLASS_IE_FIX                       : 'ie-fix',

    ID_EMAIL_ORIGIN_CONTAINER          : 'email-origin-container',
    ID_AUX_CONTAINER                   : 'aux-container',
    ID_AUX_TEMPLATES_CONTAINER         : 'fetch-aux-templates-container',
    ID_ABOOK_IMPORT_FORM               : 'abook-import-form',
    ID_IMPORT_FORM_CONTAINER           : 'fetch-import-form-container',
    ID_UPLOAD_FILE                     : 'upload-file',
    ID_UPLOAD_FILE_FORM                : 'abook-import-form-contacts-file',
    ID_INVITATION_LIST                 : 'invitation-list',
    ID_MEDIA_UPLOADER                  : 'media-server-uploader-iframe'
  },



  /*
  ******************************
  *
  * Attributes of DOM elements
  *
  *******************************
  */
  ATTR: {
    DATA_LI_EMAIL_ORIGIN               : 'data-li-email-origin',
    DATA_LI_EMAIL_DOMAIN               : 'data-li-email-domain',
    DATA_LI_IS_OAUTH                   : 'data-li-is-oauth',
    DATA_LI_IS_ASYNC                   : 'data-li-is-async',
    DATA_LI_DUMMY_EMAIL                : 'data-li-dummy-email',
    DATA_LI_UPLOAD_SUCCESS_URL         : 'data-li-upload-success-url',
    DATA_LI_LINK_TO                    : 'data-li-link-to'
  },


  /*
   *******************************
   *
   * Max number of cards to show
   * per ROW
   *
   *******************************
   */
  ITEMS_PER_ROW                        : 3,


  /*
   *******************************
   *
   * Events
   *
   *******************************
   */
  EVENTS: {
    CREATE_AUX_CONTAINER            : 'LI.ABookImportV3.CREATE_AUX_CONTAINER',
    DESTROY_AUX_CONTAINER           : 'LI.ABookImportV3.DESTROY_AUX_CONTAINER',
    START_ENTERTAINMENT             : 'LI.ABookImportV3.START_ENTERTAINMENT',
    LOADING                         : 'LI.ABookImportV3.LOADING',
    LOADED                          : 'LI.ABookImportV3.LOADED',
    RESET                           : 'LI.ABookImportV3.RESET',
    ERROR                           : 'LI.ABookImportV3.ERROR',
    SUBMIT                          : 'LI.ABookImportV3.SUBMIT'
  },


  /*
   *********************************
   *
   * Different Email Origins
   *
   *********************************
   */
  EMAIL_ORIGINS: {
    CONTACTS_FILE                   : 'contacts-file',         //manual upload contacts file
    COMPOSE_INVITE                  : 'compose-invite',        //manual textarea input
    OTHER_EMAILS                    : 'other-emails',          //other emails
    EXCHANGE                        : 'webmailImportExchange'  //exchange
  },


  /*
   ***************************************
   *
   * Content Types for Auxiliary Container
   *
   ***************************************
   */
  CONTENT_TYPES: {
    CONTACTS_FILE                   : 'contacts-file',  //manual upload contacts file
    COMPOSE_INVITE                  : 'compose-invite', //manual textarea input
    OTHER_EMAILS                    : 'other-emails',   //other emails
    EXCHANGE                        : 'webmailImportExchange',       //exchange
    ENTERTAINMENT                   : 'entertainment'        //entertainment module
  },


  /*
   *********************************
   * States of the Model
   *********************************
   */
  STATES: {
    INIT:           'INIT',
    LOADING:        'LOADING',
    LOADED:         'LOADED',
    ERROR:          'ERROR',
    ENTERTAINMENT:  'ENTERTAINMENT' //entertainment in progress
  }
};