/**
 * JS Control for ABookImportV3 view
 *
 *
 * {@jsControl name="ABookImportV3.View"}
 *   {}
 * {/jsControl}
 *
 * @class ABookImportV3.View
 * @constructor
 **/

LI.define('ABookImportV3.View');
(function(config) {

  function View(config) {

    var
        CONST         = config.consts || {},
        _css          = CONST.CSS,
        _contentTypes = CONST.CONTENT_TYPES,
        self          = this,

        DISABLED_ATTR           = 'disabled',
        SELECTOR_SUBMIT_BUTTON  = 'input[type="submit"]',

        $uploadFile                 = $('#' + _css.ID_UPLOAD_FILE),
        $uploadFileForm             = $('#' + _css.ID_UPLOAD_FILE_FORM),
        $invitationList             = $('#' + _css.ID_INVITATION_LIST),
        $emailOriginContainer       = $('#' + _css.ID_EMAIL_ORIGIN_CONTAINER),
        $cardsCont                  = $('.' + _css.CLASS_EMAIL_ORIGIN),
        $selectFile                 = $('.' + _css.CLASS_SELECT_FILE),
        $cards                      = $('.' + _css.CLASS_CARD),
        $auxContent                 = $('#' + _css.ID_AUX_CONTAINER + ' .' + _css.CLASS_CONTENT),
        $entertainment              = $('.' + _css.CLASS_AUX_TEMPLATE + '.' + _css.CLASS_ENTERTAINMENT),
        $emailOriginContainerError  = $('#' + _css.ID_EMAIL_ORIGIN_CONTAINER + ' .' + _css.CLASS_ERROR),
        $auxTemplateError           = $('.' + _css.CLASS_AUX_TEMPLATE + ' .' + _css.CLASS_ERROR),
        $auxContainer               = $('#' + _css.ID_AUX_CONTAINER),
        $auxTemplateContainer       = $('#' + _css.ID_AUX_TEMPLATES_CONTAINER),
        $contactsFile               = $('.' + _css.CLASS_AUX_TEMPLATE + '.' + _css.CLASS_CONTACTS_FILE_MODULE),
        $composeInvite              = $('.' + _css.CLASS_AUX_TEMPLATE + '.' + _css.CLASS_COMPOSE_INVITE_MODULE),
        $importInvite               = $('.' + _css.CLASS_AUX_TEMPLATE + '.' + _css.CLASS_IMPORT_INVITE_FORM_MODULE),

        $invitationListButton       = $invitationList.next(SELECTOR_SUBMIT_BUTTON),
        $uploadFileButton           = $uploadFileForm.find(SELECTOR_SUBMIT_BUTTON);


    /*
     *************************************************************
     ******************* PUBLIC METHODS **************************
     *************************************************************
     */

    /**
     * Creates the auxiliary container to host multiple content types:
     * file upload/text input/entertainment module/fetch form
     *
     * @method: createAux
     * @public
     * @param {Object} cfg - configuration object
     */
    this.createAux = function (emailOrigin) {
      var $card = $(emailOrigin.card);

      self.disableAux(); //reset for good measures

      self._moveAuxContainer(emailOrigin.card);
      self._changeAuxContent(emailOrigin.emailOrigin);
      self._displayAuxContainer();
    };

    /**
     * Disables/destroys auxiliary container
     *
     * @method: disableAux
     * @public
     */
    this.disableAux = function() {
      self._hideAuxContainer();
    };

    /**
     * Show loading state
     *
     * @method: showLoading
     * @public
     */
    this.showLoading= function() {
      self.toggleLoading($emailOriginContainer, true);
    };

    /**
     * Hide loading state
     *
     * @method: hideLoading
     * @public
     */
    this.hideLoading= function() {
      self.toggleLoading($emailOriginContainer, false);
    };

    /**
     * Adds filename to the dressed up div on
     * top of the default input field
     *
     * @method: displayFileUpload
     * @public
     * @param {Object}
     */
    this.displayFileUpload = function (data) {
      if (data && data.fileName && data.fileName !== '') {
        $selectFile.text(data.fileName);
        // Enable upload button
        $uploadFileButton.removeAttr(DISABLED_ATTR);
      } else if (data && data.title) {
        $selectFile.text(data.title);
      }
    };


    /**
     * Shows a pop up
     *
     * @method: showPopUp
     * @public
     * @param {jQuery Object}
     */
    this.showPopUp = function ($node) {
      LI.popup($node.attr('href'));
    };


    /**
     * Show Add Connections Tooltip next to email origin.
     * This only appears when they have connections not invited yet.
     *
     * @method: showAddConnectionsTooltip
     * @public
     * @param {Object}
     */
    this.showAddConnectionsTooltip = function (node) {
      $cards.removeClass(_css.CLASS_LAST_HIGHLIGHTED);
      $(node).closest('.' + _css.CLASS_CARD).addClass(_css.CLASS_LAST_HIGHLIGHTED);
    };

    /**
     * Displays the entertainment module. This requires auxiliary container to be open
     * AND graphically lays over whatever the current aux container content is,
     *
     * @method: displayEntertainment
     * @public
     */
    this.displayEntertainment= function(){
      var zindex = 'z-index';

      $auxContent.append($entertainment);

      //force displaying aux container in case aux container isn't showing
      self._displayAuxContainer();

      //set higher zindex for auxContainer for ie glitch
      //where next card is showing over aux container
      $cardsCont.css(zindex, '0');
      $auxContainer.parent().css(zindex, '1');
    };


    /**
     *
     * @method: showError
     * @public
     * @param:  {jQuery Object} $node - A dom node
     */
    this.showError = function ($node) {
      self.toggleError($node, true);
    };

    /**
     *
     * @method: injectMessage
     * @public
     * @param: {String} - message type
     */
    this.injectMessage = function (mesg) {
      if (mesg) {
        LI.injectAlert(mesg);
      }
    };

    /**
     * Removes all errors in forms
     * @method: removeAllErrors
     * @public
     */
    this.removeAllErrors = function () {
      var $formInAux = $('#' + _css.ID_EMAIL_ORIGIN_CONTAINER + ' form');

      $emailOriginContainerError.removeClass(_css.CLASS_ERROR);
      $auxTemplateError.removeClass(_css.CLASS_ERROR);
      $formInAux.removeClass(_css.CLASS_ERROR);
      LI.removeAlert();
    };

    /**
     * Resets file upload
     * http://stackoverflow.com/questions/1043957/clearing-input-type-file-using-jquery
     * Also disables upload submit button
     *
     * @method: resetFileUpload
     * @public
     */
    this.resetFileUpload = function () {

      $uploadFileButton.attr(DISABLED_ATTR, DISABLED_ATTR);

      $uploadFile.wrap('<form>').closest('form').get(0).reset();
      $uploadFile.unwrap();
    };

    /**
     * Enables/Disables the submit button for the manual invite form
     * @method toogleInvitationList
     * @param  {Boolean} flag
     * @public
     */
    this.toggleInvitationList = function (flag) {
      if ($invitationListButton) {
        if (flag) {
          $invitationListButton.removeAttr(DISABLED_ATTR);
        } else {
          $invitationListButton.attr(DISABLED_ATTR, DISABLED_ATTR);
        }
      }
    };

    /**
     * Adds/Removes error class to/from a dom node
     * @method: toggleError
     * @public
     * @param:  {jQuery Object} $node - A dom node
     * @param: {Boolean} flag - show/hide
     */
    this.toggleError = function ($node, flag) {
      $node.toggleClass(_css.CLASS_ERROR, flag);
    };

    /**
     * Adds/Removes loading class to/from a dom node
     * @method: toggleLoading
     * @public
     * @param:  {jQuery Object} $node - A dom node
     * @param: {Boolean} flag - show/hide
     */
    this.toggleLoading = function ($node, flag) {
      $node.toggleClass(_css.CLASS_LOADING, flag);
    };

    /*
     **************************************************************
     ******************* PRIVATE METHODS **************************
     **************************************************************
     */

    /**
     *
     * @method: _moveAuxContainer
     * @private
     * @param:  {Object} card - A dom node
     */
    this._moveAuxContainer= function(card){
      var
          $card                     = $(card),
          cardIdx                   = $(card.parentNode).index() + 1, // +1 to offset 0-based indexing
          $parentContainer          = $(card.parentElement),
          $prevCardContainer        = $parentContainer.prev(),
          $nextCardContainer        = $parentContainer.next();

      //Placing the aux container inside the previous card's dom IF the card is the last of a row
      //OR if the card is the last of all cards AND it's not the first of its row.
      if ( cardIdx % CONST.ITEMS_PER_ROW === 0 || (cardIdx === $emailOriginContainer.children().length && (cardIdx % CONST.ITEMS_PER_ROW > 1))) {
        $prevCardContainer.prepend($auxContainer);

      } else {
        //Otherwise place the aux container inside the card's container
        $auxContainer.insertBefore($card);
      }
    };

    /**
     *
     * @method: _changeAuxContent
     * @private
     * @param {String} contentType - which content type to paste into auxiliary container
     */
    this._changeAuxContent= function(contentType){
      //move whatever content is in #aux-container out into #fetch-import first
      $auxTemplateContainer.append($auxContent.children());

      //then append in new content
      switch (contentType) {

      case _contentTypes.CONTACTS_FILE:
        $auxContent.append($contactsFile);

        break;

      case _contentTypes.COMPOSE_INVITE:
        $auxContent.append($composeInvite);

        break;

      case _contentTypes.EXCHANGE:
        $auxContent.append($importInvite);

        /*
          Since the Exchange and "other-emails" import-forms use the same html content
          Am adding class to differentiate them, also making sure the cached content doesn't have the wrong class
         */
        if ($auxContent.hasClass(CONST.EMAIL_ORIGINS.OTHER_EMAILS)) {
          $auxContent.removeClass(CONST.EMAIL_ORIGINS.OTHER_EMAILS);
        }
        $auxContent.addClass(CONST.EMAIL_ORIGINS.EXCHANGE);
        break;

      default: //OTHER EMAILS
        $auxContent.append($importInvite);

        // Same issue as the case statement above
        if ($auxContent.hasClass(CONST.EMAIL_ORIGINS.EXCHANGE)) {
          $auxContent.removeClass(CONST.EMAIL_ORIGINS.EXCHANGE);
        }
        $auxContent.addClass(CONST.EMAIL_ORIGINS.OTHER_EMAILS);
        break;
      }
    };

    /**
     *
     * @method: _displayAuxContainer
     * @private
     */
    this._displayAuxContainer= function(){
      var
        $textArea = $('#' + _css.ID_AUX_CONTAINER + ' textarea'),
        $input = $('#' + _css.ID_AUX_CONTAINER + ' input[type="text"]'),
        $inputFile = $('#' + _css.ID_AUX_CONTAINER + ' input[type="file"]');

      $auxContainer.addClass(_css.CLASS_ON);

      //set focus correctly depending on which input is available.
      if ($textArea.length > 0 && $textArea.is(':visible')){
        $textArea.focus();
      } else if ($input.length > 0 && $input.is(':visible')){
        $input.focus();
      } else if ($inputFile.length > 0 && $inputFile.is(':visible')){
        $inputFile.focus();
      }

      $('#' + _css.ID_AUX_CONTAINER).addClass(_css.CLASS_IE_FIX); //for input field disappearing due to absolute positioning
    };

    /**
     *
     * @method:_hideAuxContainer
     * @private
     */
    this._hideAuxContainer= function(){
      $auxContainer.removeClass(_css.CLASS_ON);
    };
  }

  LI.ABookImportV3.View = new View(config);
})({consts: LI.ABookImportV3.Const});
