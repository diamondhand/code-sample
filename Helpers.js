/**
 * Helpers related to ABookImportV3
 *
 * @class ABookImportV3.Helpers
 * @constructor
 **/

LI.define('ABookImportV3.Helpers');
(function(config) {

  function Helpers(config) {

    var css = config.consts.CSS || {};

    /**
     * Is the element a card
     *
     * @method: isCard
     * @public
     * @param {Object}
     * @return {Boolean}
     */
    this.isCard = function (el){
      return ($(el).hasClass(css.CLASS_CARD)) ;
    };

    /**
     * Validates the form before submission. This only checks if
     * a text input field is empty or a textarea is empty
     *
     * @method: validateForm
     * @public
     * @param {jQuery Object} form
     * @return {Boolean}
     */
    this.validateForm = function($form) {
      var $inputs     = $form.find('input'),
          $textareas  = $form.find('textarea'),
          inputType   = 'type';

      if ($inputs.length > 0 ) {
        $inputs.each(function(){
          var $this = $(this),
              attrType = $this.attr(inputType).toLowerCase();
          if ((attrType === 'text' && $this.val() === '') ||
              (attrType === 'file' && $this.val() === '')) {
            return false;
          }
        });
      }

      if ($textareas.length > 0) {
        $textareas.each(function(){
          if ($(this).val() === '') {
            return false;
          }
        });
      }

      return true;
    };

  }

  LI.ABookImportV3.Helpers = new Helpers(config);
})({consts: LI.ABookImportV3.Const});